<?php if (!defined('THINK_PATH')) exit(); /*a:2:{s:60:"E:\tpshop\public/../application/admin\view\goods\create.html";i:1534148834;s:44:"E:\tpshop\application\admin\view\layout.html";i:1534131224;}*/ ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>后台管理系统</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link href="/static/admin/css/main.css" rel="stylesheet" type="text/css"/>
    <link href="/static/admin/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="/static/admin/css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css"/>
    <script src="/static/admin/js/jquery-1.8.1.min.js"></script>
    <script src="/static/admin/js/bootstrap.min.js"></script>

</head>
<body>
<!-- 上 -->
<div class="navbar">
    <div class="navbar-inner">
        <div class="container-fluid">
            <ul class="nav pull-right">
                <li id="fat-menu" class="dropdown">
                    <a href="#" id="drop3" role="button" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="icon-user icon-white"></i> admin
                        <i class="icon-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a tabindex="-1" href="<?php echo url('Manager/uppwd'); ?>">修改密码</a></li>
                        <li class="divider"></li>
                        <li><a tabindex="-1" href="<?php echo url('admin/login/logout'); ?>">安全退出</a></li>
                    </ul>
                </li>
            </ul>
            <a class="brand" href="index.html"><span class="first">后台管理系统</span></a>
            <ul class="nav">
                <li class="active"><a href="javascript:void(0);">首页</a></li>
                <li><a href="javascript:void(0);">系统管理</a></li>
                <li><a href="javascript:void(0);">权限管理</a></li>
            </ul>
        </div>
    </div>
</div>
<!-- 左 -->
<div class="sidebar-nav">
    <?php foreach($top_nav as $k=>$top_v): ?>
    <a href="#error-menu<?php echo $k; ?>" class="nav-header collapsed" data-toggle="collapse">
        <i class="icon-exclamation-sign"></i><?php echo $top_v['auth_name']; ?></a>
    <ul id="error-menu<?php echo $k; ?>" class="nav nav-list collapse in">
        <?php foreach($second_nav as $second_v): if(($second_v['pid'] == $top_v['id'])): ?>
        <li><a href="<?php echo url($second_v['auth_c'].'/'.$second_v['auth_a']); ?>"><?php echo $second_v['auth_name']; ?></a></li>
        <?php endif; endforeach; ?>
    </ul>
    <?php endforeach; ?>
</div>

<script type="text/javascript" charset="utf-8" src="/plugins/ueditor/ueditor.config.js"></script>
<script type="text/javascript" charset="utf-8" src="/plugins/ueditor/ueditor.all.min.js"> </script>
<!--建议手动加在语言，避免在ie下有时因为加载语言失败导致编辑器加载失败-->
<!--这里加载的语言文件会覆盖你在配置项目里添加的语言类型，比如你在配置项目里配置的是英文，这里加载的中文，那最后就是中文-->
<script type="text/javascript" charset="utf-8" src="/plugins/ueditor/lang/zh-cn/zh-cn.js"></script>

<!-- 右 -->
    <div class="content">
        <div class="header">
            <h1 class="page-title">商品新增</h1>
        </div>

        <!-- add form -->
        <form action="<?php echo url('save'); ?>" method="post" id="tab" enctype="multipart/form-data">
            <ul class="nav nav-tabs">
              <li role="presentation" class="active"><a href="#basic" data-toggle="tab">基本信息</a></li>
              <li role="presentation"><a href="#desc" data-toggle="tab">商品描述</a></li>
              <li role="presentation"><a href="#attr" data-toggle="tab">商品属性</a></li>
              <li role="presentation"><a href="#pics" data-toggle="tab">商品相册</a></li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane fade in active" id="basic">
                    <div class="well">
                        <label>商品名称：</label>
                        <input type="text" name="goods_name" value="" class="input-xlarge">
                        <label>商品价格：</label>
                        <input type="text" name="goods_price" value="" class="input-xlarge">
                        <label>商品数量：</label>
                        <input type="text" name="goods_number" value="" class="input-xlarge">
                        <label>商品logo：</label>
                        <input type="file" name="logo" value="" class="input-xlarge">
                        <label>商品分类：</label>
                        <select name="" id="cate_one" class="input-xlarge">
                            <option value="">请选着一级分类</option>
                            <?php foreach($cate_one as $v): ?>
                            <option value="<?php echo $v['id']; ?>"><?php echo $v['cate_name']; ?></option>
                            <?php endforeach; ?>
                        </select>
                        <select name="" id="cate_two" class="input-xlarge">
                            <option value="">请选着二级分类</option>
                        </select>
                        <select name="cate_id" id="cate_three" class="input-xlarge">
                            <option value="">请选着三级分类</option>
                        </select>
                    </div>
                </div>
                <div class="tab-pane fade in" id="desc">
                    <div class="well">
                        <label>商品简介：</label>
                        <textarea id="editor" name="goods_introduce" class="input-xlarge" style="width:800px;height:350px;"></textarea>
                    </div>
                </div>
                <div class="tab-pane fade in" id="attr">
                    <div class="well">
                        <label>商品类型：</label>
                        <select name="type_id" id="type_id" class="input-xlarge">
                            <option value="">请选着</option>
                            <?php foreach($list as $v): ?>
                            <option value="<?php echo $v['id']; ?>"><?php echo $v['type_name']; ?></option>
                            <?php endforeach; ?>
                        </select>
                        <div id="attr_id">
                            <!--<label>商品品牌：</label>-->
                            <!--<input type="text" name="" value="" class="input-xlarge">-->
                            <!--<label>商品型号：</label>-->
                            <!--<input type="text" name="" value="" class="input-xlarge">-->
                            <!--<label>商品重量：</label>-->
                            <!--<input type="text" name="" value="" class="input-xlarge">-->
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade in" id="pics">
                    <div class="well">
                        <div>[<a href="javascript:void(0);" class="add">+</a>]商品图片：<input type="file" name="goods_pics[]" value="" class="input-xlarge"></div>
                    </div>
                </div>
                <button class="btn btn-primary" type="submit">保存</button>
            </div>
        </form>
        <!-- footer -->
        <footer>
            <hr>
            <p>© 2017 <a href="javascript:void(0);" target="_blank">ADMIN</a></p>
        </footer>
    </div>
    <script type="text/javascript">
        $(function(){
            //实例化编辑器
            //建议使用工厂方法getEditor创建和引用编辑器实例，如果在某个闭包下引用该编辑器，直接调用UE.getEditor('editor')就能拿到相关的实例
            UE.getEditor('editor');
            $('.add').click(function(){
                var add_div = '<div>[<a href="javascript:void(0);" class="sub">-</a>]商品图片：<input type="file" name="goods_pics[]" value="" class="input-xlarge"></div>';
                $(this).parent().after(add_div);
            });
            $('.sub').live('click',function(){
                $(this).parent().remove();
            });

            //商品类二级联动效果
            //获取第一个下拉菜单,并绑定事件
            $('#cate_one').change(function () {
                //获取当前菜单栏id
                var id =  $(this).val();
                // alert(id);
                if (id == '') {
                    return;
                }
                //发送Ajax请求
                $.ajax({
                    url:"<?php echo url('admin/Category/getCateById'); ?>",
                    type:"post",
                    data:'id=' + id,
                    // dataType:,
                    success:function (msg) {
                        //清空二级,三级菜单栏
                        $('#cate_two').html("<option value=\"\">请选着二级分类</option>");
                        $('#cate_three').html("<option value=\"\">请选着三级分类</option>");
                        //判断返回数据
                        if (msg.code == 101){
                            alert(msg.log);
                             return;
                        }
                        //遍历服务器返回结果,拼接输出到界面
                        var str = '<option value="">请选着二级分类</option>';
                        $.each(msg.data,function (i, v) {
                            str += '<option value="'+v.id+'">' +v.cate_name+ '</option>';
                        })
                        $('#cate_two').html(str);
                    }
                })
            })

            //商品类三级联动菜单
            $('#cate_two').change(function () {
                //获取id
                var id = $(this).val();
                //发送ajax请求
                $.ajax({
                    url:"<?php echo url('admin/Category/getCateById'); ?>",
                    type:'post',
                    data:'id='+id,
                    dataType:'json',
                    success:function (msg) {
                        //清空三级菜单数据
                        $('#cate_three').html('<option value="">请选着三级分类</option>');
                        //判断返回结果
                        if (msg.code == 101){
                            alert(msg.log);
                            return;
                        }
                        //遍历服务器返回结果,拼接输出到界面
                        var str = '<option value="">请选着三级分类</option>';
                        $.each(msg.data,function (i, v) {
                            str += '<option value="'+v.id+'">' +v.cate_name+ '</option>';
                        })
                        $('#cate_three').html(str);
                    }
                })
            })

            //获取商品类型id并绑定事件
            $('#type_id').change(function () {
                //获取选择类型id
                var id = $(this).val();
                if (id == '') {
                    return;
                }
                //发送ajax请求
                $.ajax({
                    url:"<?php echo url('admin/attribute/getAttr'); ?>",
                    type:'post',
                    data:'id='+id,
                    dataType:'json',
                    success:function (res) {
                        if (res.code != 100) {
                            alert(res.msg);
                            return;
                        }
                        //处理服务端返回数据
                        var attr = res.data;
                        //遍历数组,拼接字符串
                        var str = '';
                        $.each(attr,function (i, v) {
                            //v.就是一条数据, 一个json格式的对象
                            str += '<label>'+ v.attr_name +'</label>';
                            if (v.attr_input_type == 0) {
                                //input 输入框
                                str += '<input type="text" name="attr_value['+ v.id +'][]" value="" class="input-xlarge">'
                            }else if (v.attr_input_type == 1) {
                                //option 下拉框
                                str += '<select name="attr_value['+ v.id +'][]" >';
                                $.each(v.attr_values,function (index, value) {
                                    str += '<option value="'+ value +'">'+ value +'</option>'
                                })
                                str += '</select>'
                            }else{
                                //多选框
                                //遍历可选参数
                                $.each(v.attr_values,function (index, value) {
                                    str += '<input type="checkbox" name="attr_value['+ v.id +'][]" value="'+ value +'">'+value;
                                })
                            }
                        })
                        //展示数据到div中
                        $('#attr_id').html(str);
                    }
                })
            })
        });
    </script>


</body>
</html>