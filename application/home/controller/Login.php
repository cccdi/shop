<?php

namespace app\home\controller;

use think\Controller;
use think\Request;

class Login extends Controller
{
    /**
     * 显示资源列表
     * 用户登录视图
     * @return \think\Response
     */
    public function login()
    {
        //
        $this->view->engine->layout(false);
        return view();
    }

    /**
     * 显示创建资源表单页.
     * 新用户注册页面
     * @return \think\Response
     */
    public function register()
    {
        //
        $this->view->engine->layout(false);
        return view();
    }

}
