<?php
namespace app\home\controller;


use think\Controller;

class Pay extends Controller
{
    //支付页视图
    public function pay()
    {
        return view();
    }
    
    //支付失败视图
    public function payfail()
    {
        return view();
    }
    
    //支付成功视图
    public function payok()
    {
        return view();
    }
}