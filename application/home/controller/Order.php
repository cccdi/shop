<?php

namespace app\home\controller;

use think\Controller;
use think\Request;

class Order extends Controller
{
    //我的订单页面
    public function index()
    {
        return view();
    }

    /**
     * 显示资源列表
     * 提交订单视图
     * @return \think\Response
     */
    public function create()
    {
        //
        return view();
    }

    //评论订单视图
    public function evaluate()
    {
        return view();
    }
}
