<?php

namespace app\home\controller;

use think\Controller;
use think\Request;

class Goods extends Controller
{
    /**
     * 显示资源列表
     * 商品列表视图
     * @return \think\Response
     */
    public function index()
    {
        //
        return view();
    }

    /**
     * 显示创建资源表单页.
     * 商品详细视图
     * @return \think\Response
     */
    public function detail()
    {
        //
        return view();
    }


}
