<?php

namespace app\admin\controller;

use think\Controller;
use think\Request;

class Attribute extends Base
{
    /**
     * 显示资源列表
     *
     * @return \think\Response
     */
    public function index()
    {
        //获取所有的类型信息  包括所属的商品分类
        $list = \app\admin\model\Attribute::alias('t1')
            ->field('t1.*,t2.type_name')
            ->join('tpshop_type t2','t1.type_id = t2.id','left')
            ->select();
        return view('index',['list'=>$list]);
    }

    /**
     * 显示创建资源表单页.
     *
     * @return \think\Response
     */
    public function create()
    {
        //获取所有分类信息
        $list = \app\admin\model\Type::select();
        return view('create',['list'=>$list]);
    }

    /**
     * 保存新建的资源
     *
     * @param  \think\Request  $request
     * @return \think\Response
     */
    public function save(Request $request)
    {
        //获取表单提交数据
        $data = $request->param();
        //参数检测
        //将数据写入数据表
        \app\admin\model\Attribute::create($data,true);
        //页面跳转
        $this->success('操作成功','index');
    }

    public function getAttr($id)
    {
        //获取当前分类下的所有类型信息
        $attr = \app\admin\model\Attribute::where('type_id',$id)->select();
        //对每一条数据进行遍历 ,取原始数据
        foreach ($attr as &$v){
            $v = $v->getData();
            //将可选值分割为数组,
            $v['attr_values'] = explode(',',$v['attr_values']);
        }
        unset($v);
        $res = [
            'code' => 100,
            'msg' => 'success',
            'data' => $attr
        ];
        return json($res);
    }
    /**
     * 显示指定的资源
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function read($id)
    {
        //
    }

    /**
     * 显示编辑资源表单页.
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * 保存更新的资源
     *
     * @param  \think\Request  $request
     * @param  int  $id
     * @return \think\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * 删除指定资源
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function delete($id)
    {
        //
    }
}
