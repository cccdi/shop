<?php

namespace app\admin\controller;

use think\Controller;
use think\Request;

class Role extends Base
{
    /**
     * 显示资源列表
     *
     * @return \think\Response
     */
    public function index()
    {
        //查询角色列表数据
        $list = \app\admin\model\Role::select();
        return view('index',['list'=>$list]);
    }

    public function setauth($id)
    {
        //获取当前角色
        $role = \app\admin\model\Role::where('id',$id)->find();
        //查询所有一级权限列表
        $list = \app\admin\model\Auth::where('pid',0)->select();
        //查询所有的二级权限
        $second_nav = \app\admin\model\Auth::where('pid','>',0)->select();
        return view('setauth',['list'=>$list,'role'=>$role,'second_nav'=>$second_nav]);
    }

    /**
     * 显示创建资源表单页.
     *
     * @return \think\Response
     */
    public function create()
    {
        //
        return view();
    }

    /**
     * 保存新建的资源
     *
     * @param  \think\Request  $request
     * @return \think\Response
     */
    public function save(Request $request)
    {
        //
    }

    /**
     * 显示指定的资源
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function read()
    {
        //接受表单提交数据
        $role_id = \request()->param('role_id');
        $auth_id = \request()->param('id/a');
        //将数组分割为字符串
        $role_auth_ids = implode(',',$auth_id);
        //数据检测
        //上传数据到表
        \app\admin\model\Role::update(['role_auth_ids'=>$role_auth_ids],['id'=>$role_id]);
        //页面跳转
        $this->success('操作成功','index');

    }

    /**
     * 显示编辑资源表单页.
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * 保存更新的资源
     *
     * @param  \think\Request  $request
     * @param  int  $id
     * @return \think\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * 删除指定资源
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function delete($id)
    {
        //
    }
}
