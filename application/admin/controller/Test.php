<?php

namespace app\admin\controller;

use think\Controller;
use think\Db;
use think\Image;
use think\Request;
use app\admin\model\Goods as GoodsModel;

class Test extends Controller
{
    /**
     * 显示资源列表
     *
     * @return \think\Response
     */
    public function index()
    {
        //获取数据
        $data = model('Goods')->select();
        //加载视图
        return view('index',['data'=>$data]);
    }

    /**
     * 显示创建资源表单页.
     *
     * @return \think\Response
     */
    public function create()
    {
        //加载试图
        return view('create');
    }

    /**
     * 保存新建的资源
     *
     * @param  \think\Request  $request
     * @return \think\Response
     */
    public function save(Request $request, $id)
    {
        //获取用户表单提交的数据
        $data = $request->param();
        //验证数据
        //商品logo图片上传
        $data['goods_logo'] = $this->upload_logo();
        //向数据表添加数据
        GoodsModel::create($data,true);
        //页面跳转
        $this->success('操作成功','goods/index');
    }

    public function upload_logo()
    {
        //获取上传的文件
        $file = \request()->file('logo');
        //判断是否上传图片
        if (empty($file)){
            $this->error('没有上传logo图');
        }
        //移动文件到指定目录下, public/upload
        $info = $file->validate(['size'=>5*1024*1024,'ext'=>'jpg,png,gif,jpeg'])->
        move(ROOT_PATH.'public'.DS.'uploads');
        //判断处理结果
        if ($info){
            //上传成功,获取文件访问路径并返回
            $goods_logo = DS.'uploads'.DS.$info->getSaveName();
            //生成缩略图
            //调用open打开一个图片
            $image = Image::open('.'.$goods_logo);
            //调用thumb生成缩略图 ,  调用save保存图片
            $image->thumb(200,200)->save('.'.$goods_logo);
            return $goods_logo;
        }else{
            //上传失败调用$file的getError方法,获取详细信息
            $err = $file->getError();
            $this->error($err);
        }
    }

    /**
     * 显示指定的资源
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function read($id)
    {
        //获取当前id值的数据
        $info = model('Goods')->find($id);
        //加载试图
        return view('read',['info'=>$info]);
    }

    /**
     * 显示编辑资源表单页.
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function edit($id)
    {
        //获取当前id值的信息
        $data = model('Goods')->find($id);
        //加载视图
        return view('edit',['data'=>$data]);
    }

    /**
     * 保存更新的资源
     *
     * @param  \think\Request  $request
     * @param  int  $id
     * @return \think\Response
     */
    public function update(Request $request, $id)
    {
        //获取用户提交的数据
        $data = input('post.');
        //检测数据
        //接受logo图片
        $file = $request->file('logo');
        //判断用户是否提交新的图片信息
        if ($file){
            $data['goods_logo'] = $this->upload_logo();
        }
        //向数据库提交数据
        model('Goods')->insert($data,[],true);
    }
    
    /**
     * 删除指定资源
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function delete($id)
    {
        //
    }


    public function Test()
    {
//        $data = Db::table('tpshop_goods')->select();
//        $data = Db::table('tpshop_goods')->
////        where('goods_name','like','%i%')->select();
//        $data = Db::table('tpshop_goods')->
//        where('goods_name|goods_price','like','%i%');
//        $data = Db::table('tpshop_goods')->where('id',34)->find();
//        $data = model('goods')->where('id',34)->find();
//        $data = \db('goods')->where('id',34)->find();
//        $data = \db('goods')->where('id',34)->value('goods_name');


//        dump($data);


        $str = '防范xss攻击<script>alert("哈哈哈哈哈哈哈哈或或")</script>';
        $str1 = remove_xss($str);

        echo $str1;
    }
}
