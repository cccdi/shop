<?php
//定义命名空间
namespace app\admin\controller;

//定义类
use app\admin\model\GoodsAttr;
use app\admin\model\Goodspics;
use think\Controller;
use app\admin\model\Goods as GoodsModel;
use think\Image;
use think\Request;

class Goods extends Base
{

    //商品列表视图
    public function index()
    {

        //查询列表页所有数据
        $list = GoodsModel::order('id desc')->paginate(10);

        return view('index', ['list' => $list]);
    }

    //商品添加试图
    public function create()
    {
        $cate = model('category')->where('pid', 0)->select();
        //获取所有的商品分类
        $list = \app\admin\model\Type::select();
        return view('create', ['cate_one' => $cate,'list'=>$list]);
    }

    //添加功能
    public function save(Request $request)
    {
        //判断接受的数据是否是post
        if (!$request->isPost()) {
            $this->error();
        }
        //接受表单提交数据
        $data = $request->param();
        //对富文本编辑器进行防止xss攻击
        $data['goods_introduce'] = $request->param('goods_introduce', 'remove_xss');
        //实例化验证类
        $validate = validate('Goods');
        //进行验证
        if (!$validate->check($data)) {
            $this->error($validate->getError());
        }
        $data = input('post.');
        $data['goods_logo'] = $this->upload_logo();
        //向数据表中写入数据
//        $res = model('Goods')->insert($data,true);
        $res = GoodsModel::create($data, true);
        //商品相册上传图片
        $this->upload_pics($res->id);
        //商品的属性值添加
        $attr_values = $data['attr_value'];
        $goodsattr_data = [];
        foreach ($attr_values as $k=>$v){
            foreach ($v as $value) {
                //组装一条数据,用于保存到数据表中
                $row = [
                    'goods_id' => $res->id,
                    'attr_id' => $k,
                    'attr_value' => $value
                ];
                $goodsattr_data[] = $row;
            }
        }
        //批量添加
        $attr =  new GoodsAttr();
        $attr->saveAll($goodsattr_data);
        //判断写入结果
        if ($res) {
            $this->success('成功', 'index');
        } else {
            $this->error('失败');
        }
    }

    //商品logo图片上传
    private function upload_logo()
    {
        //获取到上传的文件  对象
        $file = request()->file('logo');
        if (empty($file)) {
            $this->error('没有上传logo图片');
        }
        //移动文件到指定的目录
        $info = $file->
        validate(['size' => 5 * 1024 * 1024, 'ext' => 'jpg,png,gif,jpeg'])->
        move(ROOT_PATH . 'public' . DS . 'uploads');
        //判断结果并处理
        if ($info) {
            //上传成功,获取文件访问路径, 并返回
            $goods_logo = DS . 'uploads' . DS . $info->getSaveName();
//            dump($goods_logo);exit;
            //生成缩略图
            //调用open方法打开原始图片
            $image = Image::open('.' . $goods_logo);
            //调用thumb方法生成缩略图,调用save方法保存缩略图
            $image->thumb(200, 200)->save('.' . $goods_logo);
            return $goods_logo;
        } else {
            //上传失败 调用$file的getError方法,获取到具体的错误信息
            $err = $file->getError();
            $this->error($err);
        }

    }

    //商品相册图片上传
    private function upload_pics($goods_id)
    {
        //获取上传的文件,数组
        $files = request()->file('goods_pics');
        //遍历数组,对每个文件进行处理
        $goodspics_data = [];
        foreach ($files as $file) {
            //将每个文件移动到自定目录下, public/uploads/目录
            $info = $file->validate(['size' => 5 * 1024 * 1024], ['ext' => 'jpg,png,gif,jpeg'])->
            move(ROOT_PATH . 'public' . DS . 'uploads');
            if ($info) {
                //上传成功,拼接图片访问路径
                $pics_origin = DS . 'uploads' . DS . $info->getSaveName();
                //生成2张不同尺寸的缩略图 800*800  400*400
                //先确定两张缩略图的存放路径
                $temp = explode(DS, $info->getSaveName());

                $pics_big = DS . 'uploads' . DS . $temp[0] . DS . 'thumb_800_' . $temp[1];
                $pics_sma = DS . 'uploads' . DS . $temp[0] . DS . 'thumb_400_' . $temp[1];

                //生成缩略图
                $image = \think\Image::open('.' . $pics_origin);
                $image->thumb(800, 800)->save('.' . $pics_big);
                $image->thumb(400, 400)->save('.' . $pics_sma);
                //将上传图片保存到相册表
                $row = [
                    'goods_id' => $goods_id,
                    'pics_big' => $pics_big,
                    'pics_sma' => $pics_sma
                ];
                //将每一条数据保存到一个二维数组中,最后进行批量增加
                $goodspics_data[] = $row;
            }
        }
        //将所有相册数据保存到相册表
        $goods_pics = new Goodspics();
        $goods_pics->saveAll($goodspics_data);
    }

    //商品编辑试图
    public function edit($id)
    {
        //获取数据
        $data = model('goods')->find($id);
        //查询所有一级分类
        $cate_one_all = model('category')->where('pid', 0)->select();
        //查询当前三级分类信息,pid值对应的就是 ,二级分类信息
        $cate_three = model('category')->find($data['cate_id']);
        //查询商品所属的二级分类信息,就是$cate_three的cate_id
        $cate_two = model('category')->find($cate_three['pid']);
        //查询所有的二级分类信息
        $cate_two_all = model('category')->where('pid', $cate_two['pid'])->select();
        //查询所有的三级分类
        $cate_three_all = model('category')->where('pid', $cate_two['id'])->select();
        //查询相册图片
        $goodspics = Goodspics::where('goods_id', $id)->select();
        return view('edit', [
            'data' => $data,
            'cate_one_all' => $cate_one_all,
            'cate_two' => $cate_two,
            'cate_two_all' => $cate_two_all,
            'cate_three_all' => $cate_three_all,
            'goodspics' => $goodspics,
        ]);
    }

    //商品详细视图
    public function read($id)
    {
        //获取对应id数据
        $info = model('Goods')->find($id);
//        dump($info->toArray());
//        exit;
        return view('read', ['info' => $info]);
    }

    //删除方法
    public function delete($id)
    {
        //查询对应id数据
//        $data = model('goods')->find($id);
        $data = GoodsModel::find($id);
//        dump($data->toArray());exit;
        $data->delete();
        //页面跳转
        $this->success('删除成功');
    }

    //更新方法
    public function update(Request $request, $id)
    {
//        $data = model('goods')->find($id);
        //接受表单提交数据
        $data = $request->param();
        //对富文本编辑器进行防止xss攻击
        $data['goods_introduce'] = $request->param('goods_introduce', 'remove_xss');
        //对用户提交的数据进行验证
        //实例化验证类
        $validate = validate('goods');
        //进行验证
        if (!$validate->check($data)) {
            $this->error($validate->getError());
        }
        //修改商品logo图片
        //获取上传图片
        $file = request()->file('logo');
        if ($file) {
            $data['goods_logo'] = $this->upload_logo();
        }
//        dump($data->toArray());exit;
        model('goods')->update($data, ['id' => $id], true);
        //跟新上传相册图片
        $this->upload_pics($id);
        //页面跳转
        $this->success('更新成功', 'index');
    }

    //删除相册图片
    public function delpics($id)
    {
        //对Ajax请求发送id进行检测
        if (empty($id)) {
            $res = [
                'code' => 101,
                'msg' => '参数错误'
            ];
            return json($res);
        }
        //删除数据
        Goodspics::destroy($id);
        //返回结果
        $res = [
            'code' => 100,
            'msg' => 'success'
        ];
        return json($res);
    }
}