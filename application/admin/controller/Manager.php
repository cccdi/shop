<?php
//定义命名空间
namespace app\admin\controller;


//定义类
use think\Controller;

class Manager extends Base
{
    //管理员列表视图
    public function index()
    {
        return view();
    }

    //管理员添加试图
    public function create()
    {
        return view();
    }
    
    //管理更改试图
    public function edit()
    {
        return view();
    }
    
//    //权限管理列表试图
//    public function auth()
//    {
//        return view();
//    }
//
//    //新增权限试图
//    public function authAdd()
//    {
//        return view();
//    }
//
//    //更改密码
//    public function uppwd()
//    {
//        return view();
//    }
}