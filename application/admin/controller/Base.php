<?php

namespace app\admin\controller;

use think\Controller;
use think\Request;

class Base extends Controller
{
    public function __construct(Request $request)
    {
        //先实现父类的构造方法
        parent::__construct($request);
        //登录检车
        if (!session('manager_info')){
            //没有登录,跳转
            $this->redirect('admin/login/index');
        }
        //调用权限控制
        $this->getNav();
        //调用访问控制
        $this->checkauth();
    }
    
    //权限控制
    public function getNav()
    {
        //从session中获取当前角色id
        $role_id = session('manager_info.role_id');
        //对角色id进行判断,若是1  直接查询所有权限
        if (1 == $role_id){
            //超级管理员,直接查询所有权限
            //查询顶级权限
            $top_nav = \app\admin\model\Auth::where(['pid'=>0,'is_nav'=>1])->select();
            //查询二级去全能型
            $second_nav = \app\admin\model\Auth::where(['pid'=>['>',0],'is_nav'=>1])->select();
        }else{
            //其他管理员
            $role = \app\admin\model\Role::find($role_id);
            $role_auth_ids = $role['role_auth_ids'];
            //查询顶级选项
            $top_nav = \app\admin\model\Auth::where([
                'pid'=>0,
                'is_nav'=>1,
                'id'=>['in',$role_auth_ids]
            ])->select();
            //查询二级权限
            $second_nav = \app\admin\model\Auth::where([
                'pid' => ['>',0],
                'is_nav' => 1,
                'id' => ['in',$role_auth_ids]
            ])->select();
        }

        //模板变量赋值
        $this->assign('top_nav',$top_nav);
        $this->assign('second_nav',$second_nav);
    }
    
    //越权访问控制
    public function checkauth()
    {
        //从session中获取角色id
        $role_id = session('manager_info.role_id');
        //判断 id是否为1 若为1 不需要判断 超级管理员
        if (1 == $role_id){
            return;
        }
        //当前页面为首页时 也不需要检测
        //获取访问控制器方法
        $controller = \request()->controller();
        $action = \request()->action();
        if ($controller == 'Index' && $action == 'index'){
            return;
        }
        //其他情况,进行检测
        //查询当前用户所拥有的权限
        $role = \app\admin\model\Role::find($role_id);
        $role_auth_ids = $role['role_auth_ids'];
        //更具控制器的方法,查询当前访问的权限
        $auth = \app\admin\model\Auth::where([
            'auth_c' => $controller,
            'auth_a' => $action
        ])->find();
        $auth_id = $auth['id'];
        //判断当前访问的页面是否有权限
        if (!in_array($auth_id,explode(',',$role_auth_ids))){
            $this->error('没有权限访问','admin/index/index');
        }
        return;
    }
}
