<?php
namespace app\admin\controller;


use think\Controller;
use think\Validate;
use app\admin\model\Manager;

class Login extends Controller
{
    //后台登录视图
    public function index()
    {
        //
        if (request()->isGet()){
            //get请求页面展示
            //临时关闭布局模板
            $this->view->engine->layout(false);
            return view();
        }
        //post请求,表单提交
        //接受数据
        $data = request()->param();
        //参数检测
        $rule = [
            'username' => 'require',
            'password' => 'require|length:1,16',
        ];
        $msg = [
            'username.require' => '用户名不能为空',
            'password.require' => '密码不能为空',
            'password.length' => '密码长度不正确'
        ];

        $validate = new Validate($rule, $msg);
        if (!$validate->check($data)){
            $err = $validate->getError();
            $this->error($err);
        }
//        //验证码校验
//        if (!captcha_check($data['code'])){
//            //验证码错误
//            $this->error('验证码错误');
//        }
        //查询管理员表进行登录认证
        $pass = encrypt_password($data['password']);
//        $user = Manager::where('username',$data['username'])->where('password',$pass)
//            ->find();
        $where = [
          'username' => $data['username'],
          'password' => $pass
        ];
        $user = Manager::where($where)->find();
        //判断查询结果
        if ($user){
            //成功
            session('manager_info',$user->toArray());
            //页面跳转
            $this->success('登录成功','admin/index/index');
        }else{
            //失败
            //页面跳转
            $this->error('登录失败');
        }
        //页面跳转
    }
    //后台退出
    public function logout()
    {
        //清空session
        session(null);
        //跳转到登录页面
//        $this->success('admin/login/index');
        $this->redirect('admin/login/index');
    }
}