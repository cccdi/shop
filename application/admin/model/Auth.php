<?php

namespace app\admin\model;

use think\Model;

class Auth extends Model
{
    //设置修改器
    public function getIsNavAttr($value)
    {
        return $value ? '是' : '否' ;
    }
}
