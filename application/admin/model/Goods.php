<?php
namespace app\admin\model;


use think\Model;
use traits\model\SoftDelete;

class Goods extends Model
{
//    protected  $table='tpshop_goods';

    //设置软删除
    use SoftDelete;
    //设置软删除,先关字段名称
    protected $deleteTime = 'delete_time';
}