<?php
namespace  app\admin\validate;

use think\Validate;

class Goods extends  Validate
{
    //定义验证规则
    protected $rule=[
        'goods_name' => 'require',
        'goods_price' => 'require|float|gt:0',
        'goods_number' => 'require|number|gt:0',
        'cate_id' => 'require|integer|gt:0'
    ];
    //定义错误提示信息
    protected $message=[
        'goods_name.require' => '商品名称不能为空',
        'goods_price.require' => '商品价格不能为空',
        'goods_price.float' => '商品价格必须是数字',
        'goods_price.gt' => '商品价格必须大于0',
        'goods_number.require' => '商品数量不能为空',
        'goods_number.number' => '商品的数量必须是整数',
        'goods_number.gt' => '商品的数量必须大于0',
        'cate_id.require' => '商品类型不能为空',
        'cate_id.integer' => '商品分类格式不正确',
        'cate_id.gt' => '商品分类格式不正确'
    ];
    //验证场景
    protected $scene =[
        'add' => ['goods_name','goods_price','goods_number'],
        'update' => ['goods_name','goods_price','goods_number']
    ];
}